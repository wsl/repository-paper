paper = wsl-repository.pdf

epub = $(patsubst %.pdf, %.epub, $(paper))
source = $(patsubst %.pdf, %.md, %.bib, $(paper))

PANDOC = pandoc --standalone \
	--from markdown \
	--include-in-header=header.tex \
	--variable=fontsize:12pt \
	--number-sections \
	--biblio wsl-repository.bib \
	--csl=sbc.csl \
	--filter pandoc-citeproc

all: $(paper)

epub: $(epub)

$(paper): %.pdf: %.md header.tex
	$(PANDOC) --to latex --output $@ $<

$(epub): %.epub: %.md
	$(PANDOC) --to epub --output $@ $<

debug.tex: $(source)
		$(PANDOC) --to latex --output $@ $<

$(paper) $(epub): build_images

.PHONY: build_images clean_images

build_images:
	$(MAKE) -C images/

clean_images:
	$(MAKE) -C images/ clean

clean: clean_images
	$(RM) $(paper) debug.* *.epub
